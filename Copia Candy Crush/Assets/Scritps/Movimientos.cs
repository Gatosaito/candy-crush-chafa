using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Movimientos : MonoBehaviour
{
    //variable para invocar el scritp de puntaje 
    public Puntaje movimientos;

    //variable para llamar el textmeshpro de algo
    private TextMeshProUGUI TextMesh;

    //variable apra llamar al game manager y poder usar el tiempo en este scritp
    public GameManager tiempo;

    public void Start()
    {
        //aqui le digo al juego que llame dicho componente y se lo de a este scritp para ser utilizado
        TextMesh = GetComponent<TextMeshProUGUI>();
    }

    private void Update()
    {
        //convierto en formato de texto los movimeintos que le quedan al jugador
        TextMesh.text = movimientos.movimientosNecesarios.ToString("0");

        //un condicional que haga que cunedo sus movimientos llegen a cero invoque la funcion de setcondion
        if (movimientos.movimientosNecesarios < 1 || tiempo.restante < 1)
        {
            movimientos.SetCondition();
        }
    }
}
