using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    //indices X y Y
    public int xIndex;
    public int yIndex;

    //variable para el tablero
    Board m_board;

    //variables para el sonido
    private AudioSource musica;
    public AudioClip audioFX;

    //funcion que inicializa los indices y el tablero 
    public void Init(int cambioX, int cambioY, Board board) 
    {
        xIndex = cambioX;
        yIndex = cambioY;

        m_board = board;
    }

    //funciion que detecta al clickear en un tile 
    public void OnMouseDown()
    {
        m_board.ClickedTile(this);
    }

    //funcion que reconoce cuando el mouse entra en otro tile,
    public void OnMouseEnter()
    {
        m_board.DragToTile(this);
    }    

    //funcion que reconoce cunado se suelta el mouse para reporducir un sonido y efectuar las funciones antes explicadas
    public void OnMouseUp()
    {
        m_board.ReleaseTile();
        AudioSource.PlayClipAtPoint(audioFX, gameObject.transform.position);
    }

}
