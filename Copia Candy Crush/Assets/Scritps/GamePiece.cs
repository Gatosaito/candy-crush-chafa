using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePiece : MonoBehaviour
{

    //variables para los indices X y Y
    public int xIndex;
    public int yIndex;

    //aqui pido al board
    Board m_board;

    //booleano que dice si esta moviendose o no
    bool m_isMoving = false;

    //para identificar que tipo de movimiento tiene la ficha y que tipo de ficha es
    public TipoInterpolacion tipoDeInterpolo;
    public TipoFicha tipoFicha;

    //setea las coordenadas de la ficha en referente al tile que esta en la matriz
    internal void SetCoord(int x, int y)
    {
        xIndex = x;
        yIndex = y;
    }

    //inicializa el board
    internal void Init(Board board)
    {
        m_board = board;
    }

    //permite mover la ficha
    internal void Move(int x, int y, float moveTime)
    {
        if (!m_isMoving)
        {
            StartCoroutine(MoveRoutine(x, y, moveTime));
        }
    }

    //rutina que permite cambiar de coordenadas y mover la ficha en la matriz
    //aparte de determinar si es de x tipo de interpolacion hacer una formula para que su movimiento, y velocidad cambie
    IEnumerator MoveRoutine(int destX, int destY, float timeToMove)
    {
        Vector2 startPosition = transform.position;
        bool reacedDestination = false;
        float elapsedTime = 0f;
        m_isMoving = true;

        while (!reacedDestination)
        {
            if (Vector2.Distance(transform.position, new Vector2(destX, destY)) < 0.01f)
            {
                reacedDestination = true;
                if (m_board != null)
                {
                    m_board.placeGamePiece(this, destX, destY);
                }
                break;
            }

            elapsedTime += Time.deltaTime;
            float t = Mathf.Clamp(elapsedTime / timeToMove, 0f, 1f);

            switch (tipoDeInterpolo)
            {
                case TipoInterpolacion.Lineal:

                    break;

                case TipoInterpolacion.Entrada:

                    t = 1 - Mathf.Cos(t * Mathf.PI * .5f);

                    break;

                case TipoInterpolacion.Salida:

                    t = Mathf.Sin(t + Mathf.PI * .5f);

                    break;

                case TipoInterpolacion.Suavizado:

                    t = t * t * (3 - 2 * t);

                    break;

                case TipoInterpolacion.MasSuavizado:

                    t = t * t * t * (t * (t * 6 - 15) + 10);

                    break;
            }

            transform.position = Vector2.Lerp(startPosition, new Vector2(destX, destY), t);

            yield return null;
        }

        m_isMoving = false;
    }

    //los tipos de movimiento de la ficha
    public enum TipoInterpolacion
    {
        Lineal,
        Entrada,
        Salida,
        Suavizado,
        MasSuavizado,
    }
    
    //los tipo de ficha que hay
    public enum TipoFicha 
    {
        Azul,
        Verde,
        Amarillo,
        Rojo,
        Rosado,
        Celeste,
        Negro,
        Blanco,
        naranja,
    }
}
