using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Puntaje : MonoBehaviour
{
    //variable entera par apuntos
    private int puntos;

    //vatriable entera para declarar los movientos necesarios 
    public int movimientosNecesarios;
    //variable para poner un maximo de puntaje el cual conseguir
    [SerializeField] private int goalScore;

    //otra vez invocando el ext mesh pro de algo
    private TextMeshProUGUI TextMesh;

    //variable para invocar el game manager en este scritp
    public GameManager tiempo;

    //una seccion de opciones para poner y cuadrar las condiciones de victoria que tendra el jugador y setearlo en cada nivel
    public WinCondition _winCondition;

    private void Start()
    {
        //pido el text mesh pro
        TextMesh = GetComponent<TextMeshProUGUI>();
        
        //conviento los puntos en un string par apoder mostrarlos en pantalla 
        TextMesh.text = puntos.ToString("0");
    }

    //eesta funcion permite sumar los puntos para asi poder calcular la cantidad que tiene el jugador, y luego convertilo en string para mostrarlo en pantalla para que el jugador vea su puntaje
    public void SumatoriaPuntos(int puntosEntrada) 
    {
        puntos += puntosEntrada;
        TextMesh.text = puntos.ToString();
    }

    //una funcion que utiliza un switch para tener un numero de opciones que dependen de un enumerador para asi calcular y ver que opcion de victoria tiene cada nivel y de que manera debe de ganar el jugador
    public void SetCondition()
    {
        switch (_winCondition)
        {
            case WinCondition.Time:

                if (tiempo.restante >= 1 && puntos >= goalScore)
                {
                    SceneManager.LoadScene("Win");
                }
                else
                {
                    SceneManager.LoadScene("Perdida");
                }

                break;
            case WinCondition.Score:

                if (puntos >= goalScore)
                {
                    SceneManager.LoadScene("Win");
                }
                else
                {
                    SceneManager.LoadScene("Perdida");
                }

                break;
            case WinCondition.mix:

                if (tiempo.restante >= 0 && puntos >= goalScore)
                {
                    SceneManager.LoadScene("Win");
                }
                else
                {
                    SceneManager.LoadScene("Perdida");
                }

                break;
        }
    }

    //un enumerador que ayuda a seleccionar de que manera queremos que el jugador gane
    public enum WinCondition
    { 
        Time,
        Score,
        mix,
    }
}

