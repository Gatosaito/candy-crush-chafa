using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Board : MonoBehaviour
{
	//declaramos variables de ancho y largo
	public int width;
	public int height;

	//declaramos una variable para controlar el borde de la camara
	public int borderSize;

	//declaramos la variable y el array para los prefbas
	public GameObject tilePrefab;
	public GameObject[] gamePiecesPrefabs;
	
	//declaramos una variable para el tiepo que tendra el mover una pieza
	public float swapTime = .3f;


	//declaro una variable para llamar el scritp de puntaje y usarlo en este scritp
	public Puntaje m_puntaje;

	//aqui se declaran los arrays de los tiles y la fichas a usar
	Tile[,] m_allTiles;
	GamePiece[,] m_allGamePieces;

	//aqui declaramos las variables para las fichas seleccionadas
	[SerializeField] Tile m_clickedTile;
	[SerializeField] Tile m_targetTile;

	//booleano para controlar el movimiento de fichas del jugador
	bool m_playerInputEnabled = true;

	//transform para controlar la camara y otras cosas
	Transform tileParent;
	Transform gamePieceParent;

	//contador para los combos 
	int myCount = 0;

	//sonido
	private AudioSource musica;
	public AudioClip audioFX;

	private void Start()
	{
		SetParents();

		m_allTiles = new Tile[width, height];
		m_allGamePieces = new GamePiece[width, height];

		SetupTiles();
		SetupCamera();
		FillBoard(10, .5f);
	}

	//esta funcion sirve para modificar los transform de los gameobjects
	private void SetParents()
	{
		if (tileParent == null)
		{
			tileParent = new GameObject().transform;
			tileParent.name = "Tiles";
			tileParent.parent = this.transform;
		}

		if (gamePieceParent == null)
		{
			gamePieceParent = new GameObject().transform;
			gamePieceParent.name = "GamePieces";
			gamePieceParent = this.transform;
		}
	}

	//esta funcion sirve para setear la camara y modificarla en paralelo a la relacion aspeceto del juego, junto al tama�o del borde que se selecciono
	private void SetupCamera()
	{
		Camera.main.transform.position = new Vector3((float)(width - 1) / 2f, (float)(height - 1) / 2f, -10f);

		float aspectRatio = (float)Screen.width / (float)Screen.height;
		float verticalSize = (float)height / 2f + (float)borderSize;
		float horizontalSize = ((float)width / 2f + (float)borderSize) / aspectRatio;
		Camera.main.orthographicSize = verticalSize > horizontalSize ? verticalSize : horizontalSize;
	}

	//esta funcion setea los tiles, poniendolos en pantalla, ubicandolos en forma de cuadricula
	private void SetupTiles()
	{
		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				GameObject tile = Instantiate(tilePrefab, new Vector2(i, j), Quaternion.identity);
				tile.name = $"Tile({i},{j})";

				if (tileParent != null)
				{
					tile.transform.parent = tileParent;
				}
				m_allTiles[i, j] = tile.GetComponent<Tile>();
				m_allTiles[i, j].Init(i, j, this);
			}
		}
	}

	//esta funcion sirve para rellenar la matriz de piezas en las ubicacion respectivas de cada tile en la cuadricula
	//tambien tiene en cuenta el numero de veces que se puede hacer un match al comenzar, y cunado llega al numero maximo de iteracciones debuge una advertencia
	//tambien rellena la matriz al momento de que a esta le falten fichas al hacer un match
	private void FillBoard(int falseOffset = 0, float moveTime = .1f)
	{
		List<GamePiece> addedPieces = new List<GamePiece>();

		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				if (m_allGamePieces[i, j] == null)
				{
					if (falseOffset == 0)
					{
						GamePiece piece = FillRandomAt(i, j);
						addedPieces.Add(piece);
					}
					else
                    {
						GamePiece piece = FillRandomAt(i, j, falseOffset, moveTime);
						addedPieces.Add(piece);
                    }
				}
			}
		}
		int maxIterations = 20;
		int iterations = 0;

		bool isFilled = false;

		while (!isFilled)
		{
			List<GamePiece> matches = FindAllMatches();

			if (matches.Count == 0)
			{
				isFilled = true;
				break;
			}
			else
			{
				matches = matches.Intersect(addedPieces).ToList();

				if (falseOffset == 0)
				{
					ReplaceWithRandom(matches);
				}
				else
				{
					ReplaceWithRandom(matches, falseOffset, moveTime);
				}
			}

			if (iterations > maxIterations)
			{
				isFilled = true;
				Debug.LogWarning($"Board.FillBoard alcanzo el maximo de interacciones, abortar");
			}

			iterations++;
		}
	}

	//funcion que te permite seleccionar una casilla
	public void ClickedTile(Tile tile)
	{
		if (m_clickedTile == null)
		{
			m_clickedTile = tile;
		}
	}

	//esta permite arrastrar el mouse con una ficha seleccionada hacia otra ficha
	public void DragToTile(Tile tile)
	{
		if (m_clickedTile != null && IsNextTo(tile, m_clickedTile))
		{
			m_targetTile = tile;
		}
	}

	//esta funciona para utilizar las dos fichas seleccionadas para cambiarlas de sitio y resetear las fichas seleccionadas
	public void ReleaseTile()
	{
		if (m_clickedTile != null && m_targetTile != null)
		{
			SwitchTiles(m_clickedTile, m_targetTile);
		}
		m_clickedTile = null;
		m_targetTile = null;
	}

	//esta funcion activa la corrutina de switchtiles
	private void SwitchTiles(Tile m_clickedTile, Tile m_targetTile)
	{
		StartCoroutine(SwitchTilesRoutine(m_clickedTile, m_targetTile));
	}

	//esta es una corrutina que nos permite cambiar las piezas de sitio, junto a sus indices de coordenada en la matriz, tambien limita el tiempo de cambio de estas, y si se hace un match ayuda a relenar la matriz
	IEnumerator SwitchTilesRoutine(Tile clickedTile, Tile targetTile)
	{
		if (m_playerInputEnabled)
		{
			GamePiece clickedPiece = m_allGamePieces[clickedTile.xIndex, clickedTile.yIndex];
			GamePiece targetPiece = m_allGamePieces[targetTile.xIndex, targetTile.yIndex];

			if (clickedPiece != null && targetPiece != null)
			{
				clickedPiece.Move(targetTile.xIndex, targetTile.yIndex, swapTime);
				targetPiece.Move(clickedPiece.xIndex, clickedPiece.yIndex, swapTime);

			yield return new WaitForSeconds(swapTime);

				List<GamePiece> clickedPieceMatches = FindMatchesAt(clickedTile.xIndex, clickedTile.yIndex);
				List<GamePiece> targetPieceMatches = FindMatchesAt(targetTile.xIndex, targetTile.yIndex);

				if (clickedPieceMatches.Count == 0 && targetPieceMatches.Count == 0)
				{
					clickedPiece.Move(clickedTile.xIndex, clickedTile.yIndex, swapTime);
					targetPiece.Move(targetTile.xIndex, targetTile.yIndex, swapTime);

					yield return new WaitForSeconds(swapTime);

				}
                else
                {
					CleatAndRefillBoard(clickedPieceMatches.Union(targetPieceMatches).ToList());
					AudioSource.PlayClipAtPoint(audioFX, gameObject.transform.position);
                }
			}
		}
	}

	//esta funcion activa la corrutina de cleatandrefillboard y mantiene el contador de combos en cero, para que asi no se mantenga en el valor antes almacenado
	private void CleatAndRefillBoard(List<GamePiece> gamePieces)
	{
		myCount = 0;
		StartCoroutine(ClearAndRefillRoutine(gamePieces));
	}

	//esta es una lista que encuentra coincidencias en toda la matriz con un tama�o minimo para hacer coincidencia
	List<GamePiece> FindMatches(int startX, int startY, Vector2 searchDirection, int minLenght = 3)
	{
		List<GamePiece> matches = new List<GamePiece>();
		GamePiece startPiece = null;

		if (IsWithBounds(startX, startY))
		{
			startPiece = m_allGamePieces[startX, startY];
		}

		if (startPiece != null)
		{
			matches.Add(startPiece);
		}
		else
		{
			return null;
		}

		int nextX;
		int nextY;

		int maxValue = width > height ? width : height;

		for (int i = 1; i < maxValue; i++)
		{
			nextX = startX + (int)Mathf.Clamp(searchDirection.x, -1, 1) * i;
			nextY = startY + (int)Mathf.Clamp(searchDirection.y, -1, 1) * i;

			if (!IsWithBounds(nextX, nextY))
			{
				break;
			}

			GamePiece nextPiece = m_allGamePieces[nextX, nextY];

			if (nextPiece == null)
			{
				break;
			}
			else
			{
				if (nextPiece.tipoFicha == startPiece.tipoFicha && !matches.Contains(nextPiece))
				{
					matches.Add(nextPiece);
				}
				else
				{
					break;
				}
			}
		}

		if (matches.Count >= minLenght)
		{
			return matches;
		}
		else
		{
			return null;
		}
	}

	//esta encuentra coincidencias en forma vertical, combinando dos listas una que revisa hacia abajo y otra que revisa hacia arriba
	List<GamePiece> FindVerticalMatches(int startX, int startY, int minLenght = 3)
	{
		List<GamePiece> upwardMatches = FindMatches(startX, startY, Vector2.up, 2);
		List<GamePiece> downwardMatches = FindMatches(startX, startY, Vector2.down, 2);

		if (upwardMatches == null)
		{
			upwardMatches = new List<GamePiece>();
		}
		if (downwardMatches == null)
		{
			downwardMatches = new List<GamePiece>();
		}

		var combinedMatches = upwardMatches.Union(downwardMatches).ToList();
		return combinedMatches.Count >= minLenght ? combinedMatches : null;
	}

	//esta encuentra coincidencias en forma horizontal, combinando dos listas una que revisa hacia la derecha y otra que revisa hacia la izquierda
	List<GamePiece> FindHorizontalMatches(int startX, int startY, int minLenght = 3)
	{
		List<GamePiece> rightMatches = FindMatches(startX, startY, Vector2.right, 2);
		List<GamePiece> leftMatches = FindMatches(startX, startY, Vector2.left, 2);

		if (rightMatches == null)
		{
			rightMatches = new List<GamePiece>();
		}
		if (leftMatches == null)
		{
			leftMatches = new List<GamePiece>();
		}

		var combinedMatches = rightMatches.Union(leftMatches).ToList();
		return combinedMatches.Count >= minLenght ? combinedMatches : null;
	}

	//esta junta las dos listas tanto la busqueda vertical como horizontal, para crear una que busque en las dos direcciones y asi generalizar mas estas 
	//ademas de darte puntos cuando se crea una L o una T(no reconoce cuando es una L o un T)
	private List<GamePiece> FindMatchesAt(int x, int y, int minLenght = 3)
	{
		List<GamePiece> horizontalMatches = FindHorizontalMatches(x, y, minLenght);
		List<GamePiece> verticalMatches = FindVerticalMatches(x, y, minLenght);

		if (horizontalMatches == null)
		{
			horizontalMatches = new List<GamePiece>();
		}
		if (verticalMatches == null)
		{
			verticalMatches = new List<GamePiece>();
		}
        if (horizontalMatches.Count != 0 && verticalMatches.Count != 0)
        {
			int vecinos = 0;

            for (int i = 0; i < horizontalMatches.Count; i++)
            {
                if (IsNextTo(horizontalMatches[0], horizontalMatches[1]))
                {
					Debug.Log("vecinos en horizontal");
                }
            }
            for (int i = 0; i < verticalMatches.Count; i++)
            {
                if (IsNextTo(verticalMatches[0], verticalMatches[1]))
                {
					Debug.Log("vecinos verticales");
                }
            }
            if (vecinos >= 3)
            {
				Debug.Log("T");
            }
            if (vecinos == 2)
            {
				Debug.Log("L");
            }
		}

		var combinedMatches = horizontalMatches.Union(verticalMatches).ToList();
		return combinedMatches;
	}

	//revisa si hay coincidencias en la lsita de matches que se creo y almacena
	List<GamePiece> FindMatchesAt(List<GamePiece> gamePieces, int minLenght = 3)
	{
		List<GamePiece> matches = new List<GamePiece>();

		foreach (GamePiece piece in gamePieces)
		{
			matches = matches.Union(FindMatchesAt(piece.xIndex, piece.yIndex, minLenght)).ToList();

		}

		return matches;
	}

	//revisa si el vecino que tiene es del mismo tipo de ficha
	private bool IsNextTo(Tile start, Tile end)
	{
		if (Mathf.Abs(start.xIndex - end.xIndex) == 1 && start.yIndex == end.yIndex)
		{
			return true;
		}

		if (Mathf.Abs(start.yIndex - end.yIndex) == 1 && start.xIndex == end.xIndex)
		{
			return true;
		}

		return false;
	}
	private bool IsNextTo(GamePiece start, GamePiece end)
	{
		if (Mathf.Abs(start.xIndex - end.xIndex) == 1 && start.yIndex == end.yIndex)
		{
			return true;
		}

		if (Mathf.Abs(start.yIndex - end.yIndex) == 1 && start.xIndex == end.xIndex)
		{
			return true;
		}

		return false;
	}

	//este busca en la lista de todas las coincidencias todas las coincidencias que esta pueda tener, tanto en ancho como en alto
	private List<GamePiece> FindAllMatches()
	{
		List<GamePiece> combinedMatches = new List<GamePiece>();

		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				var matches = FindMatchesAt(i, j);
				combinedMatches = combinedMatches.Union(matches).ToList();
			}
		}

		return combinedMatches;
	}

	//esta funcion apaga el resaltado del tile al momento de que una coincidencia se efectue
	void HighlightTileOff(int x, int y)
	{
		SpriteRenderer spriteRender = m_allTiles[x, y].GetComponent<SpriteRenderer>();
		spriteRender.color = new Color(spriteRender.color.r, spriteRender.color.g, spriteRender.color.b, 0);
	}

	//esta funcion resalta los tiles cuando efectuas una coincidencia, y muestra el sitio donde esta sucedio
	void HighlightTileOn(int x, int y, Color col)
	{
		SpriteRenderer spriteRenderer = m_allTiles[x, y].GetComponent<SpriteRenderer>();
		spriteRenderer.color = col;
	}

	//esta resalta los tiles al momento de que se crea una coincidenia de manera natural
	void HighlightMatchesAt(int x, int y)
	{
		HighlightTileOff(x, y);
		var combinedMatches = FindMatchesAt(x, y);

		if (combinedMatches.Count > 0)
		{
			foreach (GamePiece piece in combinedMatches)
			{
				HighlightTileOn(piece.xIndex, piece.yIndex, piece.GetComponent<SpriteRenderer>().color);
			}
		}
	}

	//revisa en largo y ancho la matriz de tiles para poder resaltar los tiles al momento de que la coincidencia se realiza
	void HighlightMatches()
	{
		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				HighlightMatchesAt(i, j);
			}
		}
	}

	//esta funcion al recibir una lista realiza el resaltado del tile
	void HighlightPieces(List<GamePiece> gamepieces)
	{
		foreach (GamePiece piece in gamepieces)
		{
			if (piece != null)
			{
				HighlightTileOn(piece.xIndex, piece.yIndex, piece.GetComponent<SpriteRenderer>().color);
			}
		}
	}

	//esta funcion destruye llas fichas al momento de que se realize la coincidenciaaparte de invocar el resaltado de tiles
	void ClearPieceAt(int x, int y)
	{
		GamePiece pieceToClear = m_allGamePieces[x, y];

		if (pieceToClear != null)
		{
			m_allGamePieces[x, y] = null;

			Destroy(pieceToClear.gameObject);
		}

		HighlightTileOff(x, y);
	}

	//sobrecarga de la anterior, para revisar la lista de piezas que le damos y efectuar la eliminacion de fichas
	void ClearPieceAt(List<GamePiece> gamePieces)
	{
		foreach (GamePiece piece in gamePieces)
		{
			if (piece != null)
			{
				ClearPieceAt(piece.xIndex, piece.yIndex);
			}
		}
	}

	//revisa en ancho y largo de la matriz donde deberia de limpiarce
	void ClearBoard()
	{
		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				ClearPieceAt(i, j);
			}
		}
	}

	//esta funcion obtiene un pieza randon para colocar en la matriz
	GameObject GetRandomPiece()
	{
		int randomInx = Random.Range(0, gamePiecesPrefabs.Length);

		if (gamePiecesPrefabs[randomInx] == null)
		{
			Debug.LogWarning($"La clase Board en el array de prefabs en la posicion {randomInx} no contiene una pieza valida");
		}

		return gamePiecesPrefabs[randomInx];
	}

	//esta funcion viene y coloca en la matriz una pieza que haya seleccionado para rellenar la matriz al momento de que alguna pieza desaparezca o se vacie
	public void placeGamePiece(GamePiece gamePiece, int x, int y)
	{
		if (gamePiece == null)
		{
			Debug.LogWarning($"gamePiece invalida");
			return;
		}

		gamePiece.transform.position = new Vector2(x, y);
		gamePiece.transform.rotation = Quaternion.identity;

		if (IsWithBounds(x, y))
		{
			m_allGamePieces[x, y] = gamePiece;
		}

		gamePiece.SetCoord(x, y);
	}

	//retorna un entero que recorre el array
	private bool IsWithBounds(int x, int y)
	{
		return (x >= 0 && x < width && y >= 0 && y < height);
	}

	//esta funcion rellena la matriz con una ficha aleatoria en donde deba de ser rellenada
	GamePiece FillRandomAt(int x, int y, int falseOffset = 0, float moveTime = .1f)
	{
		GamePiece randomPiece = Instantiate(GetRandomPiece(), Vector2.zero, Quaternion.identity).GetComponent<GamePiece>();

		if (randomPiece != null)
		{
			randomPiece.Init(this);
			placeGamePiece(randomPiece, x, y);

			if (falseOffset != 0)
			{
				randomPiece.transform.position = new Vector2(x, y + falseOffset);
				randomPiece.Move(x, y, moveTime);
			}

			randomPiece.transform.parent = gamePieceParent;
		}

		return randomPiece;
	}

	//esta ffuncion remplaza fichas en la matriz 
	void ReplaceWithRandom(List<GamePiece> gamePieces, int falseOffset = 0, float moveTime = .1f)
	{
		foreach (GamePiece piece in gamePieces)
		{
			ClearPieceAt(piece.xIndex, piece.yIndex);

			if (falseOffset == 0)
			{
				FillRandomAt(piece.xIndex, piece.yIndex);
			}
			else
			{
				FillRandomAt(piece.xIndex, piece.yIndex, falseOffset, moveTime);
			}
		}
	}

	//esta permite que las columnas caigan para que asi se peuda rellenar 
	List<GamePiece> CollapseColumn(int column, float collapseTime = .1f)
	{
		List<GamePiece> movingPieces = new List<GamePiece>();

		for (int i = 0; i < height - 1; i++)
		{
			if (m_allGamePieces[column, i] == null)
			{
				for (int j = i + 1; j < height; j++)
				{
					if (m_allGamePieces[column, j] != null)
					{
						m_allGamePieces[column, j].Move(column, i, collapseTime * (j - i));
						m_allGamePieces[column, i] = m_allGamePieces[column, j];
						m_allGamePieces[column, i].SetCoord(column, i);

						if (!movingPieces.Contains(m_allGamePieces[column, i]))
						{
							movingPieces.Add(m_allGamePieces[column, i]);
						}

						m_allGamePieces[column, j] = null;
						break;
					}
				}
			}
		}

		return movingPieces;
	}

	//esta hace que las fichas se muevan hacia abajo para haer el efecto de caida
	List<GamePiece> CollapseColumn(List<GamePiece> gamePieces)
	{
		List<GamePiece> movingPieces = new List<GamePiece>();
		List<int> columnsToColapse = GetColumns(gamePieces);

		foreach (int column in columnsToColapse)
		{
			movingPieces = movingPieces.Union(CollapseColumn(column)).ToList();
		}

		return movingPieces;
	}

	//esta funcion obtiene las columnas de la matriz
	private List<int> GetColumns(List<GamePiece> gamePieces)
	{
		List<int> columns = new List<int>();

		foreach (GamePiece piece in gamePieces)
		{
			if (!columns.Contains(piece.xIndex))
			{
				columns.Add(piece.xIndex);
			}
		}

		return columns;
	}

	//esta corutina nos ayuda a limpiar y rellenar la matriz, aparte de brindar los puntos al momento de hacer una coincidencia
	IEnumerator ClearAndRefillRoutine(List<GamePiece> gamePieces)
	{
		m_playerInputEnabled = true;
		List<GamePiece> matches = gamePieces;

		do
		{
			//esta es la parte donde se activa la animacion al hacer match manual//
			foreach (GamePiece piece in matches)
			{

				if (matches.Count == 3)
                {
					int cantidadPuntos = 100;

					m_puntaje.SumatoriaPuntos(cantidadPuntos);

				}
				if (matches.Count == 4)
                {
					int cantidadPuntos = 200;

					m_puntaje.SumatoriaPuntos(cantidadPuntos);

				}
				if (matches.Count == 5)
                {
					int cantidadPuntos = 300;

					m_puntaje.SumatoriaPuntos(cantidadPuntos);

				}
				if (matches.Count >= 6)
                {
					int cantidadPuntos = 400;

					m_puntaje.SumatoriaPuntos(cantidadPuntos);

				}

				piece.GetComponentInChildren<Animator>().SetBool("Combo", true);
				

			}

			m_puntaje.movimientosNecesarios -= 1;


			yield return StartCoroutine(ClearAndCollapseRoutine(matches));
			yield return null;
			yield return StartCoroutine(RefillRoutine());
			matches = FindAllMatches();
			yield return new WaitForSeconds(.5f);
		}
		while (matches.Count != 0);
		m_playerInputEnabled = true;
	}
	
	//esta routina ayuda a limpiar la matriz al y colapsar las columnas al terminar las coincidencias y combinaciones que se realizaron
	//aparte de brindarte puntos al momento de que una coincidencia natural se genere y multiplicar el puntaje si estas aumentan
	IEnumerator ClearAndCollapseRoutine(List<GamePiece> gamePieces) 
	{
		myCount++;
		List<GamePiece> movingPieces = new List<GamePiece>();
		List<GamePiece> matches = new List<GamePiece>();
		HighlightPieces(gamePieces);
		yield return new WaitForSeconds(.5f);
		bool isFinished = false;
		

        while (!isFinished)
        {
			ClearPieceAt(gamePieces);
			yield return new WaitForSeconds(.25f);

			movingPieces = CollapseColumn(gamePieces);
            while (!IsCollapsed(gamePieces))
            {
				yield return null;
            }

			AudioSource.PlayClipAtPoint(audioFX, gameObject.transform.position);

			yield return new WaitForSeconds(.5f);

			matches = FindMatchesAt(movingPieces);

            if (matches.Count == 0)
            {
				isFinished = true;
				break;
            }
            else
            {

				//para que se active la animacion cuando se hace un match naturalmente al caer//
				foreach (GamePiece piece in matches)
				{


					if (matches.Count == 3)
					{
						int cantidadPuntos = 10 * myCount;

						m_puntaje.SumatoriaPuntos(cantidadPuntos);

					}
					if (matches.Count == 4)
					{
						int cantidadPuntos = 20 * myCount;

						m_puntaje.SumatoriaPuntos(cantidadPuntos);

					}
					if (matches.Count == 5)
					{
						int cantidadPuntos = 30 * myCount;

						m_puntaje.SumatoriaPuntos(cantidadPuntos);

					}
					if (matches.Count >= 6)
					{
						int cantidadPuntos = 40 * myCount;

						m_puntaje.SumatoriaPuntos(cantidadPuntos);

					}

					piece.GetComponentInChildren<Animator>().SetBool("Combo", true);
				}

				yield return StartCoroutine(ClearAndCollapseRoutine(matches));
            }
        }
		yield return null;
	}

	//rutina para rellenar la matriz
	IEnumerator RefillRoutine() 
	{
		FillBoard(10, .5f);
		yield return null;
	}

	//para confirmar si ya se colapsaron las columnas
	public bool IsCollapsed(List<GamePiece> gamePieces) 
	{
		foreach(GamePiece piece in gamePieces) 
		{
            if (piece != null)
            {
                if (piece.transform.position.y - (float)piece.yIndex > 0.001f)
                {
					return false;
                }
            }
		}

		return true;
	}
}   
        
